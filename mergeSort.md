# MERGE SORT PROJESI

## [16,21,11,8,12,22]

1. **Yukarıdaki dizinin sort türüne göre aşamalarını yazınız.**

- 16 21 | 11 | 8 | 12 22 en son bir hucrede bir sayi kalasiya kadar devam edecegiz.

- 16 21 | 11 | 8 | 12 22 olarak devam ediyoruz.

- 16 | 21 | 11 | 8 | 12 | 22 olarak tek hucreye kadar indik simdi siralamaya baslayacagiz.

- 16 21 | 8 11 | 12 22

- 8 11 16 21 | 12 22

- 8 11 12 16 21 22 olacak sekilde siralama islemini burada sonlandiriyoruz.

---

2. **Big-O gösterimini yazınız.**

- O(nlogn)

---

## [PATIKA.DEV](https://app.patika.dev/cnsbelirdi)
